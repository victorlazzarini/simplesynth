LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE   := _jsndobj
LOCAL_C_INCLUDES := $(LOCAL_PATH) $(LOCAL_PATH)/rfftw 
LOCAL_CFLAGS := -O3 
LOCAL_CPPFLAGS :=$(LOCAL_CFLAGS)
###

LOCAL_SRC_FILES := java_interface_wrap.cpp \
Oscil.cpp \
Oscilt.cpp \
Oscili.cpp \
PhOscili.cpp \
ADSR.cpp \
IADSR.cpp \
Buzz.cpp \
Balance.cpp \
DelayLine.cpp \
Tap.cpp \
Tapi.cpp \
Comb.cpp \
Allpass.cpp \
StringFlt.cpp \
Pluck.cpp \
VDelay.cpp \
Pitch.cpp \
Loop.cpp \
Filter.cpp \
Reson.cpp \
Lp.cpp \
ButtBP.cpp \
ButtBR.cpp \
ButtHP.cpp \
ButtLP.cpp \
Mix.cpp \
Pan.cpp \
Gain.cpp \
SyncGrain.cpp \
Interp.cpp \
Phase.cpp \
Lookup.cpp \
Lookupi.cpp \
Ring.cpp \
Rand.cpp \
Randh.cpp \
Randi.cpp \
Unit.cpp \
Ap.cpp \
Hilb.cpp \
SndIn.cpp \
SndObj.cpp \
MidiIn.cpp \
MidiMap.cpp \
Bend.cpp \
Fir.cpp \
FFT.cpp \
IFFT.cpp \
Convol.cpp \
FastOsc.cpp \
Osc.cpp \
Osci.cpp \
PVA.cpp \
IFGram.cpp \
HiPass.cpp \
LowPass.cpp \
TpTz.cpp \
PVS.cpp \
PVMorph.cpp \
PVFilter.cpp \
PVMask.cpp \
PVMix.cpp \
PVTransp.cpp \
PVBlur.cpp \
PVRead.cpp \
SinAnal.cpp \
SinSyn.cpp \
AdSyn.cpp \
ReSyn.cpp \
IFAdd.cpp \
SndRead.cpp \
SpecIn.cpp \
SpecMult.cpp \
SpecCart.cpp \
SpecCombine.cpp \
SpecInterp.cpp \
SpecPolar.cpp \
SpecSplit.cpp \
SpecThresh.cpp \
SpecVoc.cpp  \
Ptrack.cpp \
SndIO.cpp \
SndFIO.cpp \
SndWave.cpp \
SndAiff.cpp \
SndBuffer.cpp \
SndWaveX.cpp \
SndPVOCEX.cpp \
SndSinIO.cpp  \
HarmTable.cpp \
UsrHarmTable.cpp \
TrisegTable.cpp \
SndTable.cpp \
PlnTable.cpp \
HammingTable.cpp \
NoteTable.cpp \
UsrDefTable.cpp \
LoPassTable.cpp \
ImpulseTable.cpp \
SpecEnvTable.cpp  \
EnvTable.cpp \
PVEnvTable.cpp \
PVTable.cpp \
config.c  \
fcr_9.c \
fhf_6.c \
fn_8.c  \
frc_1.c  \
ftw_16.c \
ftwi_7.c \
executor.c \
fftwnd.c \
fhf_7.c \
fn_9.c  \
frc_10.c \
ftw_2.c  \
ftwi_8.c \
fcr_1.c \
fhb_10.c \
fhf_8.c \
fni_1.c \
frc_11.c \
ftw_3.c  \
ftwi_9.c \
fcr_10.c  \
fhb_16.c \
fhf_9.c fni_10.c frc_12.c ftw_32.c generic.c \
fcr_11.c  fhb_2.c fn_1.c fni_11.c frc_128.c ftw_4.c  malloc.c \
fcr_12.c  fhb_3.c fn_10.c fni_12.c frc_13.c ftw_5.c  planner.c \
fcr_128.c fhb_32.c fn_11.c fni_13.c frc_14.c ftw_6.c  putils.c \
fcr_13.c  fhb_4.c fn_12.c fni_14.c frc_15.c ftw_64.c rader.c \
fcr_14.c  fhb_5.c fn_13.c fni_15.c frc_16.c ftw_7.c  rconfig.c \
fcr_15.c  fhb_6.c fn_14.c fni_16.c frc_2.c  ftw_8.c  rexec.c \
fcr_16.c  fhb_7.c fn_15.c fni_2.c frc_3.c  ftw_9.c  rexec2.c \
fcr_2.c fhb_8.c fn_16.c fni_3.c frc_32.c ftwi_10.c rfftwf77.c \
fcr_3.c fhb_9.c fn_2.c fni_32.c frc_4.c  ftwi_16.c rfftwnd.c \
fcr_32.c  fhf_10.c fn_3.c fni_4.c frc_5.c  ftwi_2.c rgeneric.c \
fcr_4.c fhf_16.c fn_32.c fni_5.c frc_6.c  ftwi_3.c rplanner.c \
fcr_5.c fhf_2.c fn_4.c fni_6.c frc_64.c ftwi_32.c timer.c \
fcr_6.c fhf_3.c fn_5.c fni_64.c frc_7.c  ftwi_4.c twiddle.c \
fcr_64.c  fhf_32.c fn_6.c fni_7.c frc_8.c  ftwi_5.c wisdom.c \
fcr_7.c fhf_4.c fn_64.c fni_8.c frc_9.c  ftwi_6.c wisdomio.c \
fcr_8.c fhf_5.c fn_7.c fni_9.c ftw_10.c ftwi_64.c cfft.c \

LOCAL_LDLIBS := -llog

include $(BUILD_SHARED_LIBRARY)


