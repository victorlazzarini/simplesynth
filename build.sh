#!/bin/sh

export ANDROID_NDK_ROOT=$HOME/src/android-ndk-r8b

rm -rf src/JSndObj
mkdir -p src/JSndObj

swig -java -package  JSndObj -DNO_RTIO -c++ -lcarrays.i -verbose -outdir src/JSndObj -I/usr/local/include -I/System/Library/Frameworks/JavaVM.framework/Headers -I./jni -o jni/java_interface_wrap.cpp AudioDefs.i

$ANDROID_NDK_ROOT/ndk-build TARGET_PLATFORM=android-9 V=1




