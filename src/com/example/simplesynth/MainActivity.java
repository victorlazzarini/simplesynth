package com.example.simplesynth;


import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import JSndObj.*;

public class MainActivity extends Activity {
    Thread t;
	boolean isRunning = true;
	SeekBar fSlider;
	float sliderval;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        fSlider = (SeekBar) findViewById(R.id.frequency);
        
         // create a listener for the slider bar;
        OnSeekBarChangeListener listener = new OnSeekBarChangeListener() {
                public void onStopTrackingTouch(SeekBar seekBar) { }
                public void onStartTrackingTouch(SeekBar seekBar) { }
                public void onProgressChanged(SeekBar seekBar,int progress, 
                                              boolean fromUser) {
                    if(fromUser) sliderval = progress / (float) seekBar.getMax();
                }
        };
        
        
        fSlider.setOnSeekBarChangeListener(listener);
        
        t = new Thread(){
        	
        	public void run() {
        		
        		setPriority(Thread.MAX_PRIORITY);
                HarmTable harm =  new HarmTable(10000,1,1);
                Oscili mod = new Oscili(harm, 4.f, 10.f);
                Oscili osc = new Oscili(harm, 400.f, 10000.f, mod);
                int sr = (int) osc.GetSr();
                int buffsize = AudioTrack.getMinBufferSize(sr, AudioFormat.CHANNEL_OUT_MONO, 
                        AudioFormat.ENCODING_PCM_16BIT);
        		short buffer[] = new short[buffsize];
                 //create an audiotrack object
               AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sr, 
                AudioFormat.CHANNEL_OUT_MONO, 
                AudioFormat.ENCODING_PCM_16BIT, 
                buffsize, 
                AudioTrack.MODE_STREAM);
               int vsize = osc.GetVectorSize();
               int k = 0;
               audioTrack.play();
               
               while(isRunning){
            	       osc.SetFreq(440.f + 440.f*sliderval);
            		   mod.DoProcess();
            		   osc.DoProcess();
            		   for(int i=0; i < vsize; i++){
       					buffer[k] = (short) osc.Output(i);
            		    if(++k == buffsize) {
            		    	audioTrack.write(buffer, 0, buffsize);
            		    	k = 0;
            		    }
            		    	
            		   }  
               }
                  
               audioTrack.stop();
               audioTrack.release();
        	}
        	
        	
        };
        
        t.start();
        		
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
    public void onDestroy(){
    	   super.onDestroy();    
    	   isRunning = false;
    	   try {
    	     t.join();
    	   } catch (InterruptedException e) {
    	     e.printStackTrace();
    	   }    
    	  t = null;
    	}
    
    
}



